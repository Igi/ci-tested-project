<?php

namespace App\Forms;

use Nette\Application\UI\Form;
use Nette\Security\User;


class FormFactory
{
    /**
     * @var User
     */
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

	public function create() : Form
	{
		return new Form;
	}

    public function getUserId()
    {
        return $this->user->getId();
    }
}
